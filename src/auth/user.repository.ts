import * as bcrypt from "bcrypt";
import { User } from "src/entities/user.entity";
import { DataSource, Repository } from "typeorm";

import { Injectable } from "@nestjs/common";

import { CreateUserDto } from "./dto/create-user.dto";

@Injectable()
export class UserRepository extends Repository<User> {
  constructor(private dataSource: DataSource) {
    super(User, dataSource.createEntityManager());
  }
  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const { username, password, status } = createUserDto;
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    const user = this.create({ username, password: hashPassword, status });

    await this.save(user);
    return user;
  }
}
