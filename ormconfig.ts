import "reflect-metadata";
import { DataSource, DataSourceOptions } from "typeorm";

export const dataSourceOptions: DataSourceOptions = {
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "devtopia",
  password: "devtopia",
  database: "flea_market",
  entities: ["dist/src/entities/*.entity{.js,.ts}"],
  migrations: ["dist/src/migrations/*{.js,.ts}"]
};

const dataSource = new DataSource(dataSourceOptions);

export default dataSource;
